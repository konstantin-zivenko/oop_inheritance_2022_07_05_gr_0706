import hr
import employees
import productivity


manager = employees.Manager(1, "Semen Semenko", 20000)
secretary = employees.Secretary(4, "Olena Donik", 16000)
sales_gay = employees.SalesPerson(3, "Oksana Rabko", 10000, 8000)
factory_worker = employees.FactoryWorker(2, "Taras Gulchuk", 40, 500)
temporary_secretary = employees.TemporarySecretary(5, "Olga Visoka", 40, 400)

employees = [
    manager,
    secretary,
    sales_gay,
    factory_worker,
    temporary_secretary
]

productivity_system = productivity.ProductivitySystem()
productivity_system.track(employees, 40)
payroll_system = hr.PayrollSystem()
payroll_system.calculate_payroll(employees)
